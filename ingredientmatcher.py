from measurementconversion import convert_measurement_to_kg
from ingredientparser import get_ingredient_description, get_quantity, get_unit_of_measurement
from textblob import TextBlob
import pandas as pd


sharp_csv = 'data/SHARP.csv'
sharp = pd.read_csv(sharp_csv,
                    usecols=[
                        "Food item", "GHGE of 1 kg food as consumed_kgCO2eq",
                        "Land use of 1 kg food as consumed_m2_yr"
                    ])
sharp = sharp.applymap(lambda s: s.lower() if type(s) == str else s)
sharp = sharp.rename(columns={"GHGE of 1 kg food as consumed_kgCO2eq": 'GHGE',
                              "Land use of 1 kg food as consumed_m2_yr": 'Land Use'})


'''
ingredientmatcher.py

This file handles the retrieval of GHGE and LU data from the SHARP dataset. By matching the parsed raw ingredient descriptions from "ingredientparser.py" and using them as a parameter in match_ingredient().
'''


def match_ingredient(ingredient_description: str) -> pd.DataFrame:
    '''
    Performs a regex.search() on all Food Item names in the SHARP-DB, and returns a dataframe with potential matches. The first match is used by default.

    '''
    try:
        df = sharp[sharp['Food item'].str.contains(
            ingredient_description)]  # Filters the entire SHARP-DB by a RegEx match on search_parameter.
    except:
        return sharp
    return df.reset_index()


def calculate_score(raw_ingredient_string: str) -> float:
    ''' 
    Calculates the sustainability score of an ingredient string (i.e: "2 ounces of milk")
    Performs a re.search() based filter with it on the SHARP database. Calculates the availables scores found and returns the sustainability score.

    '''

    def add_sustainability_factors(amount: int, ghge: float, land_usage: float) -> float:
        return (amount*land_usage)+(amount*ghge)

    score = 0

    ingredient = get_ingredient_description(raw_ingredient_string)
    search = match_ingredient(ingredient)

    # If there's a match between recipe ingredient and SHARP-DB:
    if len(search) > 0:
        # Retrieve the sustainability properties.
        ghge = float(search.at[0, 'GHGE'])
        land_use = float(search.at[0, 'Land Use'])
        quantity = get_quantity(raw_ingredient_string)
        measurement = get_unit_of_measurement(raw_ingredient_string)
        quantity_kg = convert_measurement_to_kg(measurement, quantity)
        try:
            score = add_sustainability_factors(quantity_kg, ghge, land_use)
        except TypeError:
            return score
    return score
