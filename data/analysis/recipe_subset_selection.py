import pandas as pd

file_path = 'sustainability_scores_2.csv'


def get_recipes_with_only_full_scores(file: str = file_path):
    # Get sustainability score data.
    df = pd.read_csv(file)
    s = pd.to_numeric(df['percentage_of_ingredients_matched'], errors='coerce')

    # Round all the values up to two decimals
    s = s.map(lambda x: round(x, 2))

    # Group the values in ranges
    range_90_100 = (s[(s > 0.9)])
    return range_90_100


print(get_recipes_with_only_full_scores())
