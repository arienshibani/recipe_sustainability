import pandas as pd
import plotly.graph_objects as go
from pandas import DataFrame

file = 'sustainability_scores.csv'


def generate_bar_plot(file):
    # Get sustainability score data.
    df = pd.read_csv(file)
    s = pd.to_numeric(df['percentage_of_ingredients_matched'], errors='coerce')

    # Round all the values up to two decimals
    s = s.map(lambda x: round(x, 2))

    # Group the values in ranges
    range_0_10 = (s[s < 0.1].count())
    range_10_20 = (s[(s > 0.1) & (s < 0.2)].count())
    range_20_30 = (s[(s > 0.2) & (s < 0.3)].count())
    range_30_40 = (s[(s > 0.3) & (s < 0.4)].count())
    range_40_50 = (s[(s > 0.4) & (s < 0.5)].count())
    range_50_60 = (s[(s > 0.5) & (s < 0.6)].count())
    range_60_70 = (s[(s > 0.6) & (s < 0.7)].count())
    range_70_80 = (s[(s > 0.7) & (s < 0.8)].count())
    range_80_90 = (s[(s > 0.8) & (s < 0.9)].count())
    range_90_100 = (s[(s > 0.9)].count())

    x = ['0-10%', '10-20%', '20-30%', '30-40%', '40-50%',
         '50-60%', '60-70%', '70-80%', '80-90%', '90-100%']
    y = [range_0_10, range_10_20, range_20_30, range_30_40, range_40_50,
         range_50_60, range_60_70, range_70_80, range_80_90, range_90_100]

    # Use textposition='auto' for direct text
    fig = go.Figure(data=[go.Bar(
        x=x, y=y,
        text=y,
        textposition='auto',
    )])

    fig.update_layout(
        title="Matching The SHARP-DB with 58.000 Online recipes.",
        xaxis_title="Percentage of recipe ingredients matched",
        yaxis_title="Number of Recipes",
        legend_title="Legend Title",
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="Black"

        )
    )

    fig.show()


generate_bar_plot('sustainability_scores.csv')
generate_bar_plot('sustainability_scores_2.csv')
