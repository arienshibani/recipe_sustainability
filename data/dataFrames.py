import pandas as pd


'''Data Frames: 58k recipes + the sustainability data '''
recipes = pd.read_csv("data/recipes.csv", usecols=["URL", "Name"])
sharp = pd.read_csv("data/SHARP.csv",
                    usecols=[
                        "Food item", "GHGE of 1 kg food as consumed_kgCO2eq",
                        "Land use of 1 kg food as consumed_m2_yr"
                    ]).applymap(lambda s: s.lower() if type(s) == str else s).rename(columns={"GHGE of 1 kg food as consumed_kgCO2eq": 'GHGE',
                                                                                              "Land use of 1 kg food as consumed_m2_yr": 'Land Use'})
