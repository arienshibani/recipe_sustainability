from csv import DictReader
from os import error
from util.terminal_text import print_bold, print_green, print_red
from sustainabilitycalculator import calculate_score, get_food_match
from itertools import islice
import pandas as pd
import json
import math
import csv
import re


def write_scores_to_csv(url: str, coverage: float, score: float, output_filename: str = 'sustainability_scores_2.csv') -> None:
    '''Append a row with url + sustainability score, to a CSV file.'''

    with open(f'data/analysis/{output_filename}', mode='a', newline='') as output_file:
        writer = csv.writer(output_file)
        writer.writerow([url, score, coverage])


def compute_all_scores(index: int = 0):

    with open('data/analysis/cached_recipe_ingredients.csv', 'r') as read_obj:
        csv_reader = DictReader(read_obj)
        index = len(pd.read_csv('data/analysis/sustainability_scores_2.csv'))
        recipes_processed = index + 1

        for row in islice(csv_reader, index, None):

            sustainability_score = 0
            ingredients_matched = 0
            list_of_ingredients = eval(row[' ingredients_used'])

            for ingredient in list_of_ingredients:

                ingredient_score = calculate_score(ingredient)
                if ingredient_score > 0:  # Score = 0 means there was no match in the database.
                    ingredients_matched += 1

                sustainability_score += calculate_score(ingredient)

            try:
                write_scores_to_csv(row["recipe_url"], (ingredients_matched /
                                                        len(list_of_ingredients)), sustainability_score)
            except ZeroDivisionError:
                write_scores_to_csv(row["recipe_url"],
                                    "DivideByZeroError", sustainability_score)
            except error:
                write_scores_to_csv(row["recipe_url"],
                                    "regex.error", sustainability_score)


def test_pipeline(index: int = 0, write_scores=False):

    with open('data/analysis/cached_recipe_ingredients.csv', 'r') as read_obj:
        csv_reader = DictReader(read_obj)
        amount_of_recipes = len(pd.read_csv(
            'data/analysis/sustainability_scores.csv'))
        recipes_processed = 0

        for row in csv_reader:
            recipe_title = row["recipe_url"].split(
                '/')[-2].replace("-", " ").title()
            recipes_processed += 1
            sustainability_score = 0
            ingredients_matched = 0
            list_of_ingredients = eval(row[' ingredients_used'])

            print_bold(
                '|----------------------------------------------------------')
            print_bold(f'| Recipe {recipes_processed} / {amount_of_recipes} ')
            print_bold(f'| { recipe_title }')
            print_bold(
                '|----------------------------------------------------------')

            for ingredient in list_of_ingredients:
                ingredient_score = calculate_score(ingredient)

                # Returns 0 if it doesn't match.
                if not get_food_match(ingredient) == 0:
                    ingredients_matched += 1
                    print_green(
                        f'| Ingredient: {ingredient} |' + '\033[1m' + f'Score: {round(ingredient_score, 2)}' + '\033[m' + f' SHARP-ID: {get_food_match(ingredient)}')
                else:
                    print_red(
                        f'| Ingredient: {ingredient} | Score: Unable to match (complex string or no SHARP-ID match found).')

                if not math.isnan(calculate_score(ingredient)):
                    sustainability_score += calculate_score(ingredient)

            print_bold(
                '|----------------------------------------------------------')
            print_bold(
                f"| COMPUTED SCORE: {round(sustainability_score, 2)}")
            recipes_processed += 1
            print(
                f'| Ingredient Coverage Ratio: {ingredients_matched} / {len(list_of_ingredients)}')
            print_bold(
                '|----------------------------------------------------------')

            if write_scores:
                write_scores(row["recipe_url"], (ingredients_matched /
                                                 len(list_of_ingredients)), sustainability_score)

            next = input("| Next Recipe? (press enter)")
            if next == "json":
                print(format_recipe_to_json(
                    recipe_title, row["recipe_url"], list_of_ingredients, sustainability_score, (ingredients_matched/len(list_of_ingredients)*100)))
            print(" ")
            print(" ")


def format_recipe_to_json(title, url, ingredients, score, ingredients_matched):
    '''This function formats a recipe from the CSV file into JSON for the thesis survey application'''
    jsonObject = {'recipeTitle': title, 'recipeUrl': url,
                  'recipeImage': url, 'recipeIngredients': ingredients, 'sustainabilityScore': score, 'accuracy': ingredients_matched}

    jsonString = json.dumps(jsonObject, indent=4,
                            ensure_ascii=False).encode('utf-8')

    # Write down the recipe in JSON format:
    with open("./json.txt", "a+") as file:
        file.write(jsonString.decode()+",\n")

    return jsonString.decode()


test_pipeline(600)
