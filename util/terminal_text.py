def print_green(text: str):
    TGREEN = '\033[32m'  # Green Text
    ENDC = '\033[m'  # reset to the defaults
    print(TGREEN + text + ENDC)


def print_red(text: str):
    TRED = '\033[31m'  # Red Text
    ENDC = '\033[m'  # reset to the defaults
    print(TRED + text + ENDC)


def print_bold(text: str):
    BOLD = '\033[1m'  # Bold Text
    ENDC = '\033[m'  # reset to the defaults
    print(BOLD + text + ENDC)
