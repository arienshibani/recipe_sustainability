def pounds_to_metric(pounds: float = 1) -> float:
    '''Converts pounds to kilograms'''
    kilograms = pounds / 2.205 
    return kilograms

def cups_to_metric(cups: float = 1) -> float:
    '''Converts cups to kilograms. (Formula based on sugar) '''
    kilograms = cups / 4.960 
    return kilograms

def tablespoons_to_metric(tbsp: float = 1) -> float:
    '''Converts tablespoons to kilograms. (Formula based on water) '''
    kilograms = tbsp / 67 
    return kilograms

def teaspoons_to_metric(tsp: float = 1) -> float:
    '''Converts teaspoons to kilograms.'''
    kilograms = tsp / 123
    return kilograms

def ounce_to_metric(ounce: float = 1) -> float:
    '''Converts ounces to kilograms.'''
    kilograms = ounce / 35.274
    return kilograms

print(cups_to_metric(1.25))
print(cups_to_metric(0.25))
print(teaspoons_to_metric(1))
print(cups_to_metric(0.5))
print(ounce_to_metric(3))

