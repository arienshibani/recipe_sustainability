import unicodedata
import re

'''

Handles the parsing of raw ingredient strings from allrecipes.com, the raw description taken from the source text
can be separated into the quantity, measurement used and ingredient description by using Regular Expressions to match the strings.

'''

units = {
    'cup': {'desc': 'cup'},
    'tablespoon': {'desc': 'tbsp'},
    'teaspoon': {'desc': 'tsp'},
    'ounce': {'desc': 'oz'},
    'gram': {'desc': 'gram'},
    'pound': {'desc': 'pound'}
}


list_of_units = list(set([x for key, value in units.items()
                          for x in [key, value['desc'], key+'s', value['desc']+'s']]))
list_of_units.sort()  # Sets are unordered, sort them.
list_of_units.reverse()  # Reverse to enforce plurals being matched first with RegEx


def get_ingredient_description(raw_string: str = '100½ cups cheese', unit_exclusion_list: list = list_of_units) -> str:
    '''Retrieve the description from a raw ingredient string.

    This function attempts to seperate the amount, as well as the measurements (cups etc) used from
    a raw ingredient string from an online recipe in order to extract the ingredient description.
    RegularExpressions are used to match unwanted parts of the raw string until the description remains.

    '''

    string = re.sub(r'[\u00BC-\u00BE\u2150-\u215E\u2189]+', '', raw_string)
    string = re.sub(r'[0-9]+', '', string)
    string = re.sub('|'.join(unit_exclusion_list), '', string)

    return string


def get_quantity(raw_string: str = '100 ½ cups cheese') -> str:
    '''Retreive the specified amount of an ingredient string.

    The raw_string will be matched against two regex patterns, searching for integers and fraction symbols,
    respectively. All matches will be converted to integers, added up, and returned as a float. The number must
    be interpreted along the results of get_measurement() in order to make any meaning out of the quantity.

    '''

    quantity = 0.0

    fraction_symbols = r'[\u00BC-\u00BE\u2150-\u215E\u2189]+'  # e.g ½
    written_fractions = r'(?:[1-9][0-9]*|0)\/[1-9][0-9]*'      # e.g 1/2
    integers = r'[0-9]+'

    find_fraction_symbols = re.search(fraction_symbols, raw_string)
    find_written_fractions = re.search(written_fractions, raw_string)
    find_integers = re.search(integers, raw_string)

    if bool(find_fraction_symbols):
        quantity += unicodedata.numeric(find_fraction_symbols.group(0))

    if bool(find_integers):
        quantity += int(find_integers.group(0))

    return quantity


def get_unit_of_measurement(raw_string: str = '100½ cups cheese', unit_list: list = list_of_units) -> str:
    '''Retrieve the measurement unit of a raw ingredient string.

    Simply matches the rawstring against a RegEx pattern of all the units mentioned in "unit_list" parameter. 
    If none of the measurements are returned, the function assumes that the ingredients are measured in discreete terms (e.g "1 whole egg")

    '''

    search = re.search('|'.join(unit_list), raw_string)
    if search:
        return search.group(0)
    else:
        return 'whole'
