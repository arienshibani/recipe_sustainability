from bs4 import BeautifulSoup
import requests
import pandas as pd
import csv
import re

'''
crawler.py

The modules handles web-scraping of allrecipes.com, and is responsible for correctly scraping and caching the ingredients found in recipes from http://www.allrecipes.com
'''


recipes = pd.read_csv("data/recipes.csv", usecols=["URL", "Name"])


def scrape_ingredients(recipe_url: str) -> list:
    '''Returns a list of all ingredient strings from an allrecipe.com url

    The parameter needs to be a valid allrecipes.com recipe link. The function will then
    use beautiful soup to harvest the ingredient strings from the appropriate HTML <span> elements used.

    '''

    ingredients = []

    try:
        response = requests.get(recipe_url)
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')

        # Scrape recipe:
        ingredients = soup.find_all(
            'span', {'class': re.compile('.*ingredients-item-name.*')})

        if not ingredients:
            # Scrape alternative HTML if recipe incorporates it:
            ingredients = soup.find_all('span', itemprop='recipeIngredient')

    except requests.exceptions.RequestException as e:
        print(f'Failed to scrape: {recipe_url}')
        ingredients.append(f'connection error: {e}')

    # Returns a list of raw ingredient strings.
    return [tags.text.strip() for tags in ingredients]


def cache_recipe_ingredients(index: int = 0) -> None:
    '''Scrape the entire corpus of recipes, and save them to a persistent csv file.'''

    def write_to_ingredient_cache(url: str, ingredients: list, output_filename: str = 'cached_recipe_ingredients.csv') -> None:
        '''Append a row with url/ingredient data, to a CSV file.'''
        with open(f'data/analysis/{output_filename}', mode='a', newline='') as output_file:
            writer = csv.writer(output_file)
            writer.writerow([url, ingredients])

    index = len(pd.read_csv('data/analysis/cached_recipe_ingredients.csv'))
    print(index)

    for url in recipes['URL'][index::]:
        list_of_ingredients = scrape_ingredients(url)
        write_to_ingredient_cache(url, str(list_of_ingredients))


cache_recipe_ingredients()
