from bs4 import BeautifulSoup
import requests
import pandas as pd
import re


def scrape_ingredients(recipe_url: str) -> list:
    '''Returns a list of all ingredient strings from an allrecipe.com url

    The parameter needs to be a valid allrecipes.com recipe link. The function will then 
    use beautiful soup to harvest the ingredient strings from the appropriate HTML <span> elements used.


    '''

    ingredients = []

    try:
        response = requests.get(recipe_url)
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')

        # Scrape recipe:
        ingredients = soup.find_all(
            'span', {'class': re.compile('.*ingredients-item-name.*')})

        if not ingredients:
            # Scrape alternative HTML if recipe incorporates it:
            ingredients = soup.find_all('span', itemprop='recipeIngredient')

    except requests.exceptions.RequestException as e:
        print(f'Failed to scrape: {recipe_url}')
        ingredients.append(f'connection error: {e}')

    # Return a list of raw ingredient strings.
    return [tags.text.strip() for tags in ingredients]


print(scrape_ingredients(
    'https://www.allrecipes.com/recipe/276263/rosemary-roasted-chicken-with-apples-and-potatoes/'))
