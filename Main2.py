from bs4 import BeautifulSoup  # WebScraping.
from textblob import TextBlob  # NLP
from csv import writer
from csv import reader
import unicodedata  # Converting unicode-fraction symbols to decimals e.g "¾"
import requests
import pandas as pd
import re
import os
import csv
import time
from crawler import scrape_ingredients


ingredients_csv = "data/parsed_ingredients.csv"  # Allrecipes.com parsed SQL.
carbon_csv = "data/SHARP.csv"  # SHARP Indicators Database.
recipes_csv = "data/recipes.csv"  # 58k Recipes to compute.

'''Data Frames: 58k recipes + the sustainability data '''
recipes = pd.read_csv(recipes_csv, usecols=["URL", "Name"])
sharp = pd.read_csv(carbon_csv,
                    usecols=[
                        "Food item", "GHGE of 1 kg food as consumed_kgCO2eq",
                        "Land use of 1 kg food as consumed_m2_yr"
                    ])

# Format the dataframe a little bit.
sharp = sharp.applymap(lambda s: s.lower() if type(s) == str else s)
sharp = sharp.rename(columns={"GHGE of 1 kg food as consumed_kgCO2eq": 'GHGE',
                              "Land use of 1 kg food as consumed_m2_yr": 'Land Use'})


def match_ingredient(ingredient_description: str) -> pd.DataFrame:
    '''Searches the SHARP data for a matchingredient_matchesing ingredient, returns a dataframe with potential matches.'''
    ingredient_description = get_noun(ingredient_description)
    # Generate a new DF with potential ingredient matches.
    df = sharp[sharp['Food item'].str.contains(ingredient_description)]
    df = df.reset_index()  # Generate new indexes for all the matches.
    return df


def calculate_sustainability_score(amount: int = 1, ghge: float = 0.2, land_usage: float = 0.2) -> float:
    '''Returns a score of an ingredients sustainability'''
    return (amount*land_usage)+(amount*ghge)


def get_noun(string: str) -> str:
    '''Use Part-of-speech Tagging to retreive the first noun from a string'''
    blob = TextBlob(string)
    for word, pos in blob.tags:
        if "NN" in pos:  # We retreive the Nouns (NN) from the text string
            return word
    return string


def coverage_check(index):
    start = time.time()
    write_row_coverage("ingredients_matched",
                       "recipe_sustainability_score", "url")
    for url in recipes['URL'][index::]:
        list_of_ingredients = crawl_recipe(url)
        ingredients_matched = 0
        recipe_sustainability_score = 0

        for ingredient in list_of_ingredients:
            search = match_ingredient(detect_ingredient(ingredient).strip())
            if(len(search) > 0):

                ingredients_matched += 1
                ghge = search.at[0, 'GHGE']
                land_use = search.at[0, 'Land Use']
                amount = detect_quantity(ingredient)

                ingredient_sustainability_score = round(
                    calculate_sustainability_score(amount, ghge, land_use), 4)
                recipe_sustainability_score += ingredient_sustainability_score

        # The fraction of ingredients that found a match in the SHARP-DB.

        coverage = f'{ingredients_matched} / {len(list_of_ingredients)}'
        if ingredients_matched == 0:
            coverage = "Scraper Failed."
        write_row_coverage(coverage, recipe_sustainability_score, url)
    elapsed_time = (time.time() - start)
    write_row_coverage("TIME", "ELAPSED", str(elapsed_time))


def write_row_coverage(coverage: str = "0/0", score: str = "0.00", url: str = "default.org"):
    with open('output_1.csv', mode='a', newline='') as output_file:
        writer = csv.writer(output_file)
        writer.writerow([coverage, score, url])


def print_recipes(automatic: bool = False, write_scores: bool = False):
    terminal_width = os.get_terminal_size()
    border = terminal_width.columns
    number_of_recipes_printed = 0
    list_of_coverage = []
    list_of_scores = []

    for url in recipes['URL']:
        recipes_sustainability_score = 0
        number_of_recipes_printed += 1

        print(f"{'-'*border}")
        print('|  Fetching Ingredients.')
        print('|  URL: ' + '\033[36m' + '\033[04m' + f'{url} ' + '\033[0m')
        print(f"{'-'*border}")

        list_of_ingredients = crawl_recipe(url)
        ingredients_printed = 0
        # Amount of ingredients actually found in the DB.
        ingredients_matched = 0

        for ingredient in list_of_ingredients:
            # Adjust layout of printed borders
            terminal_width = os.get_terminal_size()
            border = terminal_width.columns - 2
            match_in_sharpDB = False

            ingredients_printed += 1
            search = match_ingredient(detect_ingredient(ingredient).strip())
            print(search)
            print(
                "\033[1m" + f'|  Recipe ({number_of_recipes_printed}/{len(recipes)})' + '\033[0m')
            print(
                "\033[1m" + f'|  Ingredient ({ingredients_printed}/{len(list_of_ingredients)})' + f' - {ingredient}' + '\033[0m')
            print(f"{'-'*border}")
            print(f'|  Matched ingredients: ')

            if(len(search) > 0):  # If there is a match for ingredient X in list_of_ingredients
                ingredients_matched += 1

                # Look up the matched items SHARP values
                first_match = search.at[0, 'Food item']
                ghge = search.at[0, 'GHGE']
                land_use = search.at[0, 'Land Use']

                amount = detect_quantity(ingredient)
                # amount = convert_to_kilograms(amount)

                # Calculate Score:
                ingredient_sustainability_score = round(
                    calculate_sustainability_score(amount, ghge, land_use), 4)
                recipes_sustainability_score += ingredient_sustainability_score

                print("\033[1m" + "\033[32m" +
                      "| SHARP-Match Found!" + '\033[0m')
                print(f'| Matched Ingredient: ' +
                      "\033[1m" + f'  {first_match}' + '\033[0m' + f' (and {len(search)-1} others)')
                print(f'| GHGE per/kg consumed:  {ghge}')
                print(f'| Land Usage per/kg consumed: {land_use}')
                print(
                    "\033[1m" + f'| Sustainability Score: {ingredient_sustainability_score}' + '\033[0m')
                print(f"{'-'*border}")

            else:
                print("\033[1m" + "\033[31m" +
                      "| No SHARP-match found." + "\033[0m")
                print(f"{'-'*border}")

            print(f'| Raw Ingredient String: {ingredient}')
            print(
                f'| Parsed Ingredient Description: {detect_ingredient(ingredient).strip()}')
            print(
                f'| Parsed Ingredient Quantity/Measure: {str(detect_quantity(ingredient))}')
            print(
                f'| Parsed Quantity Unit: {detect_measure(ingredient).strip()}')
            print(f"{'-'*border}")

            if not automatic:
                next = input(
                    f'| Parse Next ingredient? (Press Enter)  \n {"-"*border} ')

            if ingredients_printed == len(list_of_ingredients):
                ingredients_printed = 0  # Reset counter when finnished.
                print(f"{'-'*border}")
                try:
                    list_of_coverage.append(
                        f'{ingredients_matched} / {len(list_of_ingredients)}')
                    list_of_scores.append(
                        f'{round(recipes_sustainability_score, 4)}')
                except:
                    list_of_coverage.append('Error')
                    list_of_coverage.append('Error')

                print('last recipes coverage',
                      list_of_coverage[len(list_of_coverage)-1])
                print('last recipes sustainability score',
                      list_of_scores[len(list_of_scores)-1])

        print(f"{'-'*border}")

    if write_scores:  # Add a list as column
        add_column_in_csv('data/recipes.csv', 'output_4.csv', lambda row, line_num: row.append(
            'Coverage') if line_num == 1 else row.append(list_of_coverage[line_num - 1]))


def get_list_of_unit_names() -> list:
    # Dictionary of units. TODO - Add attributes for conversion
    units = {
        'cup': {'desc': 'cup'},
        'tablespoon': {'desc': 'tbsp'},
        'teaspoon': {'desc': 'tsp'},
        'ounce': {'desc': 'ounce'},
        'gram': {'desc': 'gram'},
        'pound': {'desc': 'pound'}
    }

    ex_list = []
    for i in units.keys():
        ex_list.append(i)
        ex_list.append(units[i]['desc'])
        # Add the "s" affixes to every entry in the unit dict in order to cover their plural variants.
        ex_list.append(i+'s')
        ex_list.append(units[i]['desc']+'s')

    return ex_list


def crawl_recipe(recipe_url: str) -> []:
    '''Returns a list of all ingredients + amount used, in a recipe from allrecipes.com TODO - Make alternative crawler'''
    r = requests.get(recipe_url)  # Send an HTTP request
    html = r.text
    soup = BeautifulSoup(html, 'html.parser')
    ingredients = soup.find_all(
        'span', {'class': re.compile('.*ingredients-item-name.*')
                 })  # Retrieve a list of all the ingredients in recipe URL.
    return [tags.text.strip() for tags in ingredients]


def contains_fraction(ingredient_string: str) -> bool:
    '''Returns True/False if a string contains a fraction symbol  (i.e ¼, ½ etc.'''
    fractions = r"[\u00BC-\u00BE\u2150-\u215E\u2189]+"  # Unicode range of all fractions
    return bool(re.search(fractions, ingredient_string))


def contains_integers(ingredient_string: str) -> bool:
    '''Returns True/False if a string contains a fraction symbol  (i.e ¼, ½ etc.'''
    fractions = r"[0-9]+"  # Unicode range of all fractions
    return bool(re.search(fractions, ingredient_string))


def detect_quantity(ingredient_string: str) -> str:
    '''Returns specified amount of an ingredient used in an ingredient string'''
    fractions = r"[\u00BC-\u00BE\u2150-\u215E\u2189]+"  # RegEx pattern for unicode fractions
    quantity = 0

    # Check if there is a fraction, if so convert it to decimals and add to quantity.
    if contains_fraction(ingredient_string):
        quantity += fraction_to_decimal(re.search(fractions,
                                                  ingredient_string).group(0))

    # Some ingredient strings only describe their quantities in fractions, so we need to check for this
    if contains_integers(ingredient_string):
        quantity += int(re.search(r'[0-9]+', ingredient_string).group(0))

    return quantity


def fraction_to_decimal(fraction_symbol: str = '¾') -> float:
    '''Formats unicode fractions to deicmal, e.g: (¾ -> 0.75)'''
    fraction_symbol = u''+fraction_symbol  # Convert to unicode-string
    return float(re.sub(r'[\u00BC-\u00BE\u2150-\u215E\u2189]+', str(unicodedata.numeric(fraction_symbol)), fraction_symbol))


"butter"


def detect_ingredient(ingredient_string: str) -> str:
    string = ingredient_string

    # Removes fractions
    string = re.sub(r'[\u00BC-\u00BE\u2150-\u215E\u2189]+', '', string)

    # Removes the number used to symbolize the ingredient amount.
    string = re.sub(r'[0-9]+', '', string)

    # Removes the amount/unit from the ingredient string
    exclusion_list = get_list_of_unit_names()
    # | indicates a disjunction in regex, so x|y|z matches x or y or z.
    exclusions = '|'.join(exclusion_list)
    # Removes any matched unit/measurment that is matched.
    ingredient = re.sub(exclusions, '', string)

    return ingredient


def detect_measure(ingredient_string: str) -> str:
    '''Returns the measurement unit of an ingredient string from a recipe. '''
    search = re.search('|'.join(get_list_of_unit_names()), ingredient_string)
    if search:
        return search.group(0)
    else:
        return 'whole'


def add_column_in_csv(input_file, output_file, transform_row) -> None:
    """This function iterates over each row of the input_file and read the contents of each row as a list. Then it passes that list into a transform_row() function for modification."""
    # Open the input_file in read mode and output_file in write mode
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = writer(write_obj)
        # Read each row of the input csv file as list
        for row in csv_reader:
            # Pass the list / row in the transform function to add column text for this row
            transform_row(row, csv_reader.line_num)
            # Write the updated row / list to the output file
            csv_writer.writerow(row)


coverage_check(12683)
